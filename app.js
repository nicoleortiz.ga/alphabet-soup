const { once } = require('events');
const { createReadStream } = require('fs');
const { createInterface } = require('readline');

(async function readFile() {
  let index = 0;
  const firstLine = [];
  const alphabetSoup = {
    searchGrid: [],
    wordsToBeFound: [],
    rows: 0,
    columns: 0
  };

  try {
    const rl = createInterface({
      input: createReadStream('words.txt'),
      crlfDelay: Infinity
    });

    rl.on('line', line => {
      if (firstLine.length === 0) {
        firstLine.push(...line);
        [alphabetSoup.rows, , alphabetSoup.columns] = firstLine; //skip x value in the array
      } else {
        if (index < alphabetSoup.rows) {
          alphabetSoup.searchGrid.push([...line.split(' ').join('')]);
          index++;
        } else {
          alphabetSoup.wordsToBeFound.push(line);
        }
      }
    });
    await once(rl, 'close');
  } catch (err) {
    console.error(err);
  }
})();
